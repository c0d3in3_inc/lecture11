package com.c0d3in3.lecture11

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object{
        const val REQUEST_CODE = 22
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init(){
        updateProfile(User().firstName, User().lastName, User().email, User().year, User().gender)
        changeProfileButton.setOnClickListener {
            changeProfile()
        }
    }

    private fun changeProfile(){
        val intent = Intent(this, ProfileChangeActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE)
    }

    private fun updateProfile(firstName : String, lastName : String,  email : String, birthdayYear : Int, gender : String) {
        firstNameTextView.text = "Firstname: $firstName"
        lastNameTextView.text = "Lastname: $lastName"
        emailTextView.text = "Email: $email"
        birthYearTextView.text = "Birth year: ${birthdayYear.toString()}"
        genderTextView.text = "Gender: $gender"
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
            val userModel = data?.extras!!.get("userModel") as UserModel
            updateProfile(userModel.firstName, userModel.lastName, userModel.email, userModel.birthdayYear, userModel.gender)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}

class User{
    var firstName = "Tedo"
    var lastName = "Manvelidze"
    var email = "tedex.manvelidze@gmail.com"
    var year = 2000
    var gender = "Male"
}
