package com.c0d3in3.lecture11

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_profile_change.*
import java.time.Year

class ProfileChangeActivity : AppCompatActivity() {

    private lateinit var email : EditText
    private lateinit var firstName : EditText
    private lateinit var lastName : EditText
    private lateinit var year : EditText
    private lateinit var gender: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_change)

        init()
    }

    private fun init(){
        email = emailEditText
        firstName = firstNameEditText
        lastName = lastNameEditText
        year = yearEditText
        gender = genderEditText
        saveChangesButton.setOnClickListener {
            saveChanges()
        }
    }

    private fun saveChanges(){
        if(checkFields()){
            val userModel = UserModel(firstName.text.toString(), lastName.text.toString(),email.text.toString(), year.text.toString().toInt(), gender.text.toString())
            intent.putExtra("userModel", userModel)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
        else Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show()
    }

    private fun checkFields(): Boolean {
        if(checkYear() && isEmailValid(email.text.toString()) && lastName.text.toString().isNotEmpty()
            && firstName.text.toString().isNotEmpty() && checkGender()) return true
        return false
    }

    private fun checkYear() : Boolean{
        if(year.text.isNotEmpty() && year.text.toString().toInt() in 1..2999) return true
        else Toast.makeText(this, "Year must be in range 0-3000", Toast.LENGTH_SHORT).show()
        return false
    }

    private fun checkGender() : Boolean{
        if(gender.text.toString().toLowerCase() == "male" || gender.text.toString().toLowerCase() == "female" || gender.text.toString().toLowerCase() == "other") return true
        else Toast.makeText(this, "Gender must be equal: Male, Female, or Other", Toast.LENGTH_SHORT).show()
        return false
    }

    private fun isEmailValid(email: String): Boolean {
        if(email.isNotEmpty()) return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        else Toast.makeText(this, "Email isn't valid!", Toast.LENGTH_SHORT).show()
        return false
    }
}
